import java.util.Scanner;

public class HelloWorld {	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String name;
		
		System.out.print("Enter your name: ");
		name = sc.nextLine();
		if(name.isEmpty()) {
			System.out.print("Name must not be empty");
		} else {
			System.out.print("Hello " + name);
		}

	}

}
