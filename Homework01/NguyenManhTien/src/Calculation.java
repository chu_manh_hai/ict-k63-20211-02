import java.util.Scanner;

public class Calculation {

	public static void main(String[] args) {
		double x, y;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter two double numbers (separeted by a space): ");
		x = sc.nextDouble();
		y = sc.nextDouble();
		
		System.out.println("Sum: " + (x+y));
		System.out.println("Subtract: " + (x-y));
		System.out.println("Multiplication: " + (x*y));
		if(y == 0) {
			System.out.print("Invalid for division");
		} else {
			System.out.print("Division: " + (x/y));
		}

	}

}
