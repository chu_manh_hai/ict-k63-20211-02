import java.util.Scanner;

public class Calculation {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the first number: ");
		double a = sc.nextDouble();
		System.out.print("Enter the second number: ");
		double b = sc.nextDouble();
		
		System.out.println("Sum: " + (a+b) + "\nSubtract: " + (a-b) + "\nMultiplication: " + (a*b));
		if(b == 0) {
			System.out.print("Invalid for division");
		} else {
			System.out.print("Division: " + (a/b));
		}
	}
}