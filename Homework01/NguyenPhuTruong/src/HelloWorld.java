import java.util.Scanner;

public class HelloWorld {	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String name;
		do {
			System.out.print("Enter your name: ");
			name = sc.nextLine();
			if (name.isEmpty()) {
				System.out.print("Name must not be empty. ");
			}
		} while (name.isEmpty());
		
		System.out.println("Hello " + name);
	}
}
