1. Task assignment:
   - Design: everyone
   - Database construction: Nguyễn Mạnh Tiến
   - Code base construction (screen communicate and navigation): Chu Mạnh Hải
   - Project sum-up: Nguyễn Phú Trường
   - Use case assignment: + Station list & Station detail: Nguyen Tuan Dung
			  + Rent bike: Nguyen Manh Tien
			  + Return bike: Nguyen Phu Truong
		          + Add new bike: Chu Manh Hai

2. Team members' contribution:
   - Nguyễn Mạnh Tiến: 30%
   - Chu Mạnh Hải, Nguyễn Phú Trường: 25%
   - Nguyễn Tuấn Dũng: 20%